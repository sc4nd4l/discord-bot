module.exports = {
  disconnect: ['Bot.Disconnect'],
  guildBandAdd: [],
  guildBanRemove: [],
  guildCreate: [],
  guildDelete: [],
  guildMemberAdd: ['Bot.GuildMemberAdd'],
  guildMemberRemove: [],
  guildUnavailable: [],
  guildUpdate: [],
  message: ['Bot.Message', 'Bot.SharedCommandHandler'],
  rateLimit: [],
  ready: ['Bot.Ready'],
  reconnecting: [],
  resume: [],
  userUpdate: []
};
